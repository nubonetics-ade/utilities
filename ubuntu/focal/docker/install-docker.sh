#!/bin/bash

# Installing Docker
sudo apt install docker.io

# To run Docker without sudo, you need to run `sudo usermod -aG docker ${USER}`

# You need to logout and login or run: `su - ${USER}`
