#!/bin/bash

sudo apt-get remove -y docker docker-engine docker.io docker-ce
sudo snap remove --purge docker
sudo rm `which docker`