#!/bin/bash

sudo apt install -y fuse
sudo modprobe fuse
sudo groupadd fuse

sudo usermod -a -G fuse $(whoami)