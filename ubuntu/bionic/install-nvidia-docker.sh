#!/bin/bash

distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

cd /etc/apt/sources.list.d/; rm docker.list*
cd
sudo apt-get update && sudo apt-get install -y nvidia-container-toolkit
sudo systemctl restart docker