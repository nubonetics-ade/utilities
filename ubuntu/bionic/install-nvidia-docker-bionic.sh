#!/bin/bash
docker volume ls -q -f driver=nvidia-docker | xargs -r -I{} -n1 docker ps -q -a -f volume={} | xargs -r docker rm -f
sudo apt-get purge nvidia-docker
sudo apt-get install nvidia-docker2
sudo pkill -SIGHUP dockerd
