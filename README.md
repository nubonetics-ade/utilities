# Ubuntu 20.04 (Focal) utilities

## NVIDIA

- Install NVIDIA drivers

  `curl -fsSL https://gitlab.com/nubonetics-ade/utilities/-/raw/master/NVIDIA/install-nvidia-drivers.sh | bash`

## Docker

- Install Docker:

  `curl -fsSL https://gitlab.com/nubonetics-ade/utilities/-/raw/master/focal/install-docker.sh | bash`

- Test Docker:

  `curl https://test.docker.com | bash`

- Uninstall Docker:

  `curl -fsSL https://gitlab.com/nubonetics-ade/utilities/-/raw/master/focal/uninstall-docker.sh | bash`

- Install NVIDIA Docker:

  `curl -fsSL https://gitlab.com/nubonetics-ade/utilities/-/raw/master/focal/install-nvidia-docker.sh | bash`

- Test NVIDIA Docker:

  `curl -fsSL https://gitlab.com/nubonetics-ade/utilities/-/raw/master/test-nvidia-docker.sh | bash`

- Install ADE:

  `curl -fsSL https://gitlab.com/nubonetics-ade/utilities/-/raw/master/install-ade.sh | bash`

## SSH

- Make SSH key:

  `curl -fsSL https://gitlab.com/nubonetics-ade/utilities/-/raw/master/make-ssh-key.sh | bash`

- Add SSH connection to a host:

  `curl -fsSL https://gitlab.com/nubonetics-ade/utilities/-/raw/master/add-ssh-connection.sh | bash`

# General utilities

- Install Visual Studio Code:

  `curl -fsSL https://gitlab.com/nubonetics-ade/utilities/-/raw/master/install-vscode.sh | bash`

- Install Docker:

  `curl -fsSL https://gitlab.com/nubonetics-ade/utilities/-/raw/master/install-docker.sh | bash`

- Install NVIDIA Docker: 

  `curl -fsSL https://gitlab.com/nubonetics-ade/utilities/-/raw/master/install-nvidia-docker.sh | bash`

- Install RStudio

  `curl https://gitlab.com/nubonetics-ade/utilities/-/raw/master/install-rstudio.sh | bash`

- Install Kompose

  `curl https://gitlab.com/nubonetics-ade/utilities/-/raw/master/install-kompose.sh | bash`

- Install claat:

  `curl -fsSL https://gitlab.com/nubonetics-ade/utilities/-/raw/master/install-claat.sh | bash`

- Install Node and npm:

  `curl https://gitlab.com/nubonetics-ade/utilities/-/raw/master/install-node.sh | bash`

- Install Yarn:

  `curl -fsSL https://gitlab.com/nubonetics-ade/utilities/-/raw/master/install-yarn.sh | bash`

- Install FUSE:

  `curl https://gitlab.com/nubonetics-ade/utilities/-/raw/master/install-fuse.sh | bash`

- Install nteract: 

  `curl https://gitlab.com/nubonetics-ade/utilities/-/raw/master/install-nteract.sh | bash`

- Install Jupyter KaTeX extension:

  `curl https://gitlab.com/nubonetics-ade/utilities/-/raw/master/install-jupyter-katex.sh | bash`

- Install NIX:

  `sh <(curl https://nixos.org/nix/install) --daemon`

- Install minikube:

  `curl https://gitlab.com/nubonetics-ade/utilities/-/raw/master/install-minikube.sh | bash`

- Install kubectl:

  `curl https://gitlab.com/nubonetics-ade/utilities/-/raw/master/install-kubectl.sh | bash`

- Install kubeflow:

  `curl https://gitlab.com/nubonetics-ade/utilities/-/raw/master/install-kubeflow-minikube.sh | bash`

- Install KiCad:

  `curl https://gitlab.com/nubonetics-ade/utilities/-/raw/master/install-kicad.sh | bash`

- Install Zotero

  `curl https://gitlab.com/nubonetics-ade/utilities/-/raw/master/install-zotero.sh | bash`