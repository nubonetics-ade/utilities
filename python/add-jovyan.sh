#/bin/bash
echo 'ALL ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
NB_UID=1000
NB_USER=jovyan
groupadd --gid ${NB_UID}  ${NB_USER} 
useradd --comment "Default user" \
        --create-home \
        --gid ${NB_UID} \
        --no-log-init \
        --shell /bin/bash \
        --uid ${NB_UID} \
        ${NB_USER}
