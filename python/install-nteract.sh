#!/bin/bash
# Nubonetics.com

VERSION=${VERSION:-0.22.4}

sudo apt-get update
sudo apt-get install -y fuse
sudo wget -P /usr/local/bin https://github.com/nteract/nteract/releases/download/v0.22.4/nteract-$VERSION.AppImage
sudo chmod +x /usr/local/bin/nteract-$VERSION.AppImage
sudo mv /usr/local/bin/nteract-$VERSION.AppImage /usr/local/bin/nteract