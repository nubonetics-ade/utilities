#!/bin/bash

sudo apt-get update
sudo apt-get install libboost-all-dev

#curl https://hub.unity3d.com/linux/keys/public | gpg --dearmor > unity3d.gpg
gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv BE3E6EA534E8243F
gpg --export --armor BE3E6EA534E8243F > unity3d.gpg
sudo mv unity3d.gpg /etc/apt/trusted.gpg.d/

sudo sh -c 'echo "deb https://hub.unity3d.com/linux/repos/deb stable main" > /etc/apt/sources.list.d/unityhub.list'
sudo apt update
sudo apt-get install unityhub