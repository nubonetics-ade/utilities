#!/bin/bash

sudo apt-get update
sudo apt-get install -y curl
curl -sL https://deb.nodesource.com/setup_10.x | bash
sudo apt-get install -y nodejs npm software-properties-common
npm config set prefix ~/.local
echo "PATH=~/.local/bin/:$PATH" >> ~/.bashrc