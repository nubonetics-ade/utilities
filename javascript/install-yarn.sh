# Yarn on Ubuntu 18.04
# ref: https://linuxize.com/post/how-to-install-yarn-on-ubuntu-18-04/

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt update
sudo apt install yarn