#!/bin/bash

#ref: https://www.kicad-pcb.org/download/ubuntu/

sudo add-apt-repository --yes ppa:js-reynaud/kicad-5.1
sudo apt update
sudo apt install --yes --install-recommends kicad
# If you want demo projects
sudo apt install --yes kicad-demos