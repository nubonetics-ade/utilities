#!/bin/bash

# Ref: https://cloud.google.com/sdk/docs/install

curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-354.0.0-linux-x86_64.tar.gz
tar -xzf google-cloud-sdk-354.0.0-linux-x86_64.tar.gz
rm google-cloud-sdk-354.0.0-linux-x86_64.tar.gz

# Now run: ./google-cloud-sdk/install.sh
