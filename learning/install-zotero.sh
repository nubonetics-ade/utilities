#!/bin/bash

sudo mkdir /opt/zotero
wget https://download.zotero.org/client/release/5.0.86/Zotero-5.0.86_linux-x86_64.tar.bz2
sudo chown $USER:sudo /opt/zotero
chmod 750 -R /opt/zotero
sudo tar -xvf ./Zotero-5.0.86_linux-x86_64.tar.bz2 -C /opt/zotero/
rm Zotero-5.0.86_linux-x86_64.tar.bz2
sudo sed -i 's@\$(dirname \$(readlink -f %k))@/opt/zotero/Zotero_linux-x86_64@g' /opt/zotero/Zotero_linux-x86_64/zotero.desktop
mkdir -p ~/.local/share/applications
ln -sf /opt/zotero/Zotero_linux-x86_64/zotero.desktop ~/.local/share/applications/zotero.desktop
