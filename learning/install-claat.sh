#!/bin/bash

sudo wget -P /usr/local/bin https://github.com/googlecodelabs/tools/releases/download/v2.2.0/claat-linux-amd64
sudo mv /usr/local/bin/claat-linux-amd64 /usr/local/bin/claat
sudo chmod +x /usr/local/bin/claat
