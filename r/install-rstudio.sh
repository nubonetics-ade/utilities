#!/bin/bash

#ref: https://linuxconfig.org/how-to-install-rstudio-on-ubuntu-20-04-focal-fossa-linux

sudo apt update
sudo apt -y install r-base gdebi-core
wget https://download1.rstudio.org/desktop/bionic/amd64/rstudio-1.3.1056-amd64.deb
sudo gdebi -y rstudio-1.3.1056-amd64.deb
rm rstudio-1.3.1056-amd64.deb