#!/bin/bash

# Reference 1: https://illya13.github.io/RL/tutorial/2020/04/27/installing-tensorflow-in-docker-on-ubuntu-20.html
# Reference 2: https://pypi.org/project/ai-benchmark/

docker run -it --rm --gpus all tensorflow/tensorflow:latest-gpu /bin/bash -c "pip install ai-benchmark; python3 -c \"from ai_benchmark import AIBenchmark; benchmark = AIBenchmark(); results = benchmark.run()\""