#!/bin/bash

echo "Website:"
read HOST_URL

echo "Email:"
read USER_EMAIL

echo "filename:"
read FILENAME

ssh-keygen -t rsa -b 4096 -C $USER_EMAIL -f ~/.ssh/$FILENAME
chmod 600 ~/.ssh/$FILENAME
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/$FILENAME

cat <<EOT >> ~/.ssh/config
Host $HOST_URL
  HostName $HOST_URL
  User git
  IdentityFile ~/.ssh/$FILENAME
  IdentitiesOnly yes

EOT

cat ~/.ssh/$FILENAME.pub
