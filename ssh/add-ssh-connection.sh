#!/bin/bash

echo "Host Name:"
read HOST < /dev/tty

echo "Host URL:"
read HOST_URL < /dev/tty

echo "User:"
read USER < /dev/tty

echo "Email:"
read USER_EMAIL < /dev/tty

echo "filename:"
read FILENAME < /dev/tty

ssh-keygen -t rsa -b 4096 -C $USER_EMAIL -f ~/.ssh/$FILENAME
chmod 600 ~/.ssh/$FILENAME
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/$FILENAME

cat <<EOT >> ~/.ssh/config
Host $HOST
  HostName $HOST_URL
  User $USER
  IdentityFile ~/.ssh/$FILENAME
  IdentitiesOnly yes

EOT

ssh-copy-id $USER@$HOST_URL
