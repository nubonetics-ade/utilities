#!/bin/bash
sudo rm -f /usr/local/bin/ade
sudo apt-get install musl
sudo ln -s /lib/ld-musl-x86_64.so.1 /lib/libc.musl-x86_64.so.1
wget https://gitlab.com/nubonetics-ade/ade-cli/-/jobs/850306416/artifacts/raw/dist/ade+x86_64
sudo mv ade+x86_64 ade
chmod +x ade
sudo mv ade /usr/local/bin
echo "ADE installed in /usr/local/bin."