#!/bin/bash

# Reference: https://itsfoss.com/install-git-ubuntu/

sudo add-apt-repository ppa:git-core/ppa
sudo apt update
sudo apt install git
