#!/bin/bash

KUBEFLOW_VER=${KUBEFLOW_VER:-v1.0.2}
export BASE_DIR=/usr/local/kubeflow/$KUBEFLOW_VER

if ! command -v kfctl > /dev/null 2>&1; then
    wget https://github.com/kubeflow/kfctl/releases/download/$KUBEFLOW_VER/kfctl_$KUBEFLOW_VER-0-ga476281_linux.tar.gz
    tar -xvf kfctl_$KUBEFLOW_VER-0-ga476281_linux.tar.gz
    sudo mkdir -p $BASE_DIR
    sudo mv kfctl $BASE_DIR
    rm  kfctl_$KUBEFLOW_VER-0-ga476281_linux.tar.gz
fi

export KF_NAME=kubeflow
export KF_DIR=${BASE_DIR}/${KF_NAME}
export CONFIG_URI="https://raw.githubusercontent.com/kubeflow/manifests/v1.0-branch/kfdef/kfctl_k8s_istio.v1.0.2.yaml" 

sudo mkdir -p ${KF_DIR}
wget $CONFIG_URI
kfctl apply -f kfctl_k8s_istio.v1.0.2.yaml

# Open localhost:8080 after this:
kubectl port-forward svc/istio-ingressgateway -n istio-system 8080:80