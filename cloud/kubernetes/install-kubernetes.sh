#!/bin/bash

# Install Kubernetes
# Reference: https://ubuntu.com/kubernetes/install#single-node
# You can assign a channel: sudo snap install microk8s --classic --channel=1.20
# Reference Kubeflow issue: https://github.com/kubeflow/kubeflow/issues/5429
sudo snap install microk8s --classic

# Install kubectl
# Reference: https://kubernetes.io/docs/tasks/tools/install-kubectl/
snap install kubectl --classic

# Install Helm
# Reference: https://helm.sh/docs/intro/install/
sudo snap install helm --classic
