#!/bin/bash

# ref: https://minikube.sigs.k8s.io/docs/start/

curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_1.9.2-0_amd64.deb
sudo dpkg -i minikube_1.9.2-0_amd64.deb