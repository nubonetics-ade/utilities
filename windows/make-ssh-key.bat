# Reference: https://code.visualstudio.com/docs/remote/troubleshooting

@echo off
set /p HOST_URL="Enter Website (github.com): "

@echo off
set /p USER_EMAIL="Enter Email: "

@echo off
set /p FILENAME="Enter file name: "

ssh-keygen -t rsa -b 4096 -C %USER_EMAIL% -f C:\Users\%USERNAME%\.ssh\%FILENAME%

ssh-agent

ssh-add C:\Users\%USERNAME%\.ssh\%FILENAME%